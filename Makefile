vpath %.adoc src
vpath %.xml public
vpath %.css styles
vpath %.sty styles

.PHONY: all
all: mathproofs.pdf mathproofs.html mathproofs.tex

mathproofs.html: mathproofs.adoc colony.css partials/complex.adoc partials/others.adoc partials/probability.adoc partials/sets.adoc
	bundle exec asciidoctor -a stylesheet=colony.css -a stylesdir=styles $< -o public/$@

mathproofs.pdf: mathproofs.xml mytheme.sty
	dblatex --texstyle=styles/mytheme.sty --xsl-user=styles/mytheme.xsl -b xetex public/$<

mathproofs.xml: mathproofs.adoc partials/complex.adoc partials/others.adoc partials/probability.adoc partials/sets.adoc
	bundle exec asciidoctor -b docbook $< -o public/$@

mathproofs.tex: mathproofs.xml mytheme.sty
	dblatex --texstyle=styles/mytheme.sty --xsl-user=styles/mytheme.xsl -b xetex public/$< -t tex -o public/$@

.PHONY: clean
clean:
	rm public/*.xml
	rm public/*.pdf