<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:template match="formalpara">
    <xsl:choose>
      <xsl:when test="@role = 'definition'">
        <xsl:text>&#10;\begin{definition}</xsl:text>
        <xsl:apply-templates select="title" mode="format.title"/>
        <xsl:text>&#10;</xsl:text>
        <xsl:apply-templates select="*[not(self::title)]"/>
        <xsl:text>&#10;\end{definition}</xsl:text>
      </xsl:when>
      <xsl:when test="@role = 'theorem'">
        <xsl:text>&#10;\begin{theorem}</xsl:text>
        <xsl:apply-templates select="title" mode="format.title"/>
        <xsl:text>&#10;</xsl:text>

        <xsl:apply-templates select="*[not(self::title)]"/>
        <xsl:text>&#10;\end{theorem}&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="@role = 'lemma'">
        <xsl:text>&#10;\begin{lemma}</xsl:text>
        <xsl:apply-templates select="title" mode="format.title"/>
        <xsl:text>&#10;</xsl:text>

        <xsl:apply-templates select="*[not(self::title)]"/>
        <xsl:text>&#10;\end{lemma}&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="@role = 'proof'">
        <xsl:text>\begin{prf}</xsl:text>
        <xsl:apply-templates select="title" mode="format.title"/>
        <xsl:text>&#10;</xsl:text>

        <xsl:apply-templates select="*[not(self::title)]"/>

        <xsl:text>&#10;\end{prf}&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>&#10;{\bf </xsl:text>
        <xsl:call-template name="normalize-scape">
          <xsl:with-param name="string" select="title"/>
        </xsl:call-template>
        <xsl:text>} </xsl:text>
        <xsl:call-template name="label.id"/>
        <xsl:apply-templates/>
        <xsl:text>&#10;</xsl:text>
        <xsl:text>&#10;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!--
    setting for display mode delimiters
  -->

  <xsl:template match="alt" mode="latex">
  <xsl:variable name="delim">
    <xsl:call-template name="pi.texmath_delimiters"/>
  </xsl:variable>

  <xsl:variable name="tex">
    <xsl:variable name="text" select="normalize-space(.)"/>
    <xsl:variable name="len" select="string-length($text)"/>
    <xsl:choose>
    <xsl:when test="$delim='user'">
      <xsl:copy-of select="."/>
    </xsl:when>
    <xsl:when test="ancestor::equation[not(child::title)]">
      <!-- Remove any math mode in an equation environment -->
      <xsl:choose>
      <xsl:when test="starts-with($text,'$') and
                      substring($text,$len,$len)='$'">
        <xsl:copy-of select="substring($text, 2, $len - 2)"/>
      </xsl:when>
      <xsl:when test="(starts-with($text,'\[') and
                       substring($text,$len - 1,$len)='\]') or
                      (starts-with($text,'\(') and
                       substring($text,$len - 1,$len)='\)')">
        <xsl:copy-of select="substring($text, 3, $len - 4)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <!-- Test to be DB5 compatible, where <alt> can be in other elements -->
    <xsl:when test="ancestor::equation or
                    ancestor::informalequation or
                    ancestor::inlineequation">
      <!-- Keep the specified math mode... -->
      <xsl:variable name="displayed_eq_envs" select="'
        equation equation* align align* gather gather* alignat alignat*
        multline multline* flalign flalign*'"/>
      <xsl:variable name="displayed_eq_ed_envs" select="'
        aligned alignedat cases gathered split
      '"/>
      <xsl:variable name="env_name" select="substring-before(
        substring($text, string-length('\begin{') + 1),
        '}'
      )"/>
      <xsl:variable name="expected_closing_tag" select="substring(
        $text,
        $len - string-length(concat('\end{', $env_name, '}')) + 1
      )"/>

      <xsl:choose>
      <xsl:when test="(starts-with($text,'\[') and
                       substring($text,$len - 1,$len)='\]') or
                      (starts-with($text,'\(') and
                       substring($text,$len - 1,$len)='\)') or
                      (starts-with($text,'$') and
                       substring($text,$len,$len)='$') or
                      (starts-with($text,concat('\begin{',$env_name,'}')) and
                       $expected_closing_tag = concat('\end{',$env_name,'}') and
                       contains($displayed_eq_envs, $env_name))">
        <xsl:copy-of select="$text"/>
      </xsl:when>
      <xsl:when test="(starts-with($text,concat('\begin{',$env_name,'}')) and
                       $expected_closing_tag = concat('\end{',$env_name,'}') and
                       contains($displayed_eq_ed_envs, $env_name))">
        <xsl:copy-of select="concat('$$', $text, '$$')"/>
      </xsl:when>
      <!-- ...Or wrap in default math mode -->
      <xsl:otherwise>
        <xsl:copy-of select="concat('$', $text, '$')"/>
      </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise/>
    </xsl:choose>
  </xsl:variable>
  <!-- Encode it properly -->
  <xsl:call-template name="scape-encode">
    <xsl:with-param name="string" select="$tex"/>
  </xsl:call-template>
</xsl:template>

</xsl:stylesheet>