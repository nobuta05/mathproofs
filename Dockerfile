FROM alpine:latest

RUN apk update && apk upgrade && \
    apk --no-cache add python3 py3-pip make curl gawk alpine-sdk && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    pip install awscli

# awscli for deploying to s3

# ruby
RUN apk --no-cache add ruby && gem install bundler && \
    echo "export PATH=$(ruby -e 'print Gem.user_dir')/bin:$PATH" >> /root/.profile

# dblatex
RUN apk --no-cache add font-noto-cjk ttf-dejavu && \
    apk --no-cache add docbook-xml docbook-xsl libxslt imagemagick && \
    apk --no-cache add texlive texlive-xetex && \
    apk --no-cache add texmf-dist-bibtexextra texmf-dist-langjapanese texmf-dist-science texmf-dist-latexextra texmf-dist-pictures texmf-dist-formatsextra

RUN curl -OL https://sourceforge.net/projects/mcj/files/fig2dev-3.2.7b.tar.xz && \
    tar xJf fig2dev-3.2.7b.tar.xz && \
    cd fig2dev-3.2.7b && ./configure && make -j && make install-strip && \
    cd / && \
    rm -rf fig2dev-3.2.7b fig2dev-3.2.7b.tar.xz

RUN apk --no-cache add mercurial && \
    mkdir -p /usr/share/xml/docbook/stylesheet/docbook-xsl && \
    cp -r /usr/share/xml/docbook/xsl-stylesheets-1.79.2/* /usr/share/xml/docbook/stylesheet/docbook-xsl && \
    hg clone http://hg.code.sf.net/p/dblatex/dblatex-py3 dblatex-dblatex-py3 && \
    cd dblatex-dblatex-py3 && python3 setup.py install --catalogs=/etc/xml/docbook-xml && \
    cd / && \
    rm -rf dblatex-dblatex-py3